﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NetFwTypeLib;
using System.Reflection;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using LiveCharts;
using LiveCharts.Wpf;

namespace com_test
{

    public partial class Client : Form
    {
        #region Variables
        private Socket _ownSender;
        private IPEndPoint _endPoint = default;
        private TcpListener _serverSocket = default;
        private TcpClient _clientSocket = default;
        private readonly int _buffersize = 8192;
        private readonly int _timeout = 2000; //in ms
        private bool _connect = true;
        private int _UDPport = 0;
        private Thread _UDPListen = null;
        #endregion

        public Client()
        {
            InitializeComponent();
        }


        private void Client_Load(object sender, EventArgs e)
        {
            #region Firewallhandling
            if (FirewallHelper.Instance.IsFirewallEnabled)
            {
                Debug("Firewall Detected");
                try
                {
                    string filename = Assembly.GetEntryAssembly().Location;
                    FirewallHelper.Instance.GrantAuthorization(filename, "Ground Station");
                    if (FirewallHelper.Instance.HasAuthorization(filename))
                    {
                        Debug("Authorization granted for: " + filename);
                        Output("Firewall detected and Authorization added");
                    }
                }
                catch (Exception ex)
                {
                    Debug("Couldn´t authorize" + ex);
                }
            }
            else
                Debug("No Firewall detected");
            #endregion
        }

        #region Outputs
        /// <summary>
        /// Output in userinterface
        /// </summary>
        private void Output(string output)
        {
            MethodInvoker LabelUpdate = delegate
            {
                listBox1.Items.Add(">> " + output);
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
                listBox1.SelectedIndex = -1;
            };
            Invoke(LabelUpdate);
        }

        /// <summary>
        /// Output in userinterface
        /// </summary>
        private void UDPOutput(string output)
        {
            MethodInvoker LabelUpdate = delegate
            {
                listBox2.Items.Add(">> " + output);
                listBox2.SelectedIndex = listBox2.Items.Count - 1;
                listBox2.SelectedIndex = -1;
            };
            Invoke(LabelUpdate);
        }

        /// <summary>
        /// Output in console
        /// </summary>
        private void Debug(string output)
        {
            Extensions.Instance.Debug(output);
        }
        #endregion

        #region UDP
        private void B_UDP_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(TXT_port.Text, out _UDPport))
                Debug("undefined Port");
            if (_UDPListen != null && _UDPListen.IsAlive)
            {
                Debug("Aborting old UDP Listener");
                _UDPListen.Abort();
                _UDPListen.Join();
            }
            ThreadStart myThreadDelegate = new ThreadStart(UDPListener);
            _UDPListen = new Thread(myThreadDelegate);
            _UDPListen.Start();

            //_UDPListen = new Thread(UDPListener);
            //_UDPListen.Start();
        }

        private void UDPListener()
        {
            UdpClient listener = new UdpClient(_UDPport);
            try
            {
                Output("Waiting for UDP broadcast on Port: " + _UDPport);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, _UDPport);
                while (true)
                {                    
                    byte[] bytes = listener.Receive(ref groupEP);
                    Output($"Received broadcast from {groupEP}:" + Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                }
            }
            catch (SocketException e)
            {
                Debug("UDP Listener failed with: " + e);
            }
            catch(ThreadAbortException)
            {
                Debug("Aborting old UDP Listener");
            }
            finally
            {
                listener.Close();
            }
        }
        /*
        private Socket serverSocket = null;
        private List<EndPoint> clientList = new List<EndPoint>();
        private byte[] byteData = new byte[1024];
        private int port = 1517;                     
        private void button1_Click(object sender, EventArgs e)
        {
            this.serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            this.serverSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            this.serverSocket.Bind(new IPEndPoint(IPAddress.Any, this.port));
            EndPoint newClientEP = new IPEndPoint(IPAddress.Any, 0);
            this.serverSocket.BeginReceiveFrom(this.byteData, 0, this.byteData.Length, SocketFlags.None, ref newClientEP, DoReceiveFrom, newClientEP);
        }
        private void DoReceiveFrom(IAsyncResult iar)
        {
            try
            {
                EndPoint clientEP = new IPEndPoint(IPAddress.Any, 0);
                int dataLen = 0;
                byte[] data = null;
                try
                {
                    dataLen = this.serverSocket.EndReceiveFrom(iar, ref clientEP);
                    data = new byte[dataLen];
                    Array.Copy(this.byteData, data, dataLen);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    EndPoint newClientEP = new IPEndPoint(IPAddress.Any, 0);
                    this.serverSocket.BeginReceiveFrom(this.byteData, 0, this.byteData.Length, SocketFlags.None, ref newClientEP, DoReceiveFrom, newClientEP);
                }

                if (!this.clientList.Any(client => client.Equals(clientEP)))
                    this.clientList.Add(clientEP);
                //     DataList.Add(Tuple.Create(clientEP, data));
                string s = Encoding.ASCII.GetString(data);
            }
            catch (ObjectDisposedException)
            {
            }
        }
        public void Stop()
        {
            this.serverSocket.Close();
            this.serverSocket = null;
            this.clientList.Clear();
        }*/
        #endregion
        
        private void B_connect_Click(object sender, EventArgs e)
        {
            #region read User Input
            if (!int.TryParse(TXT_port.Text, out var port))
                Debug("undefined Port");
            IPAddress address;
            try
            {
                address = IPAddress.Parse(TXT_ip.Text);
            }
            catch (Exception)
            {
                Debug("Es wurde eine ungültige IP Adresse angegeben");
                return;
            }
            #endregion
            _connect = true;
            _endPoint = new IPEndPoint(address, port);
            var Connection = new Thread(StayConnected);
            Connection.Start();
        }

        private void StayConnected()
        {
            if (!Connect())
            {
                string msg = "Do you want to continue trying to connect to: " + _endPoint.Address + ":" + _endPoint.Port + "?";
                DialogResult result = MessageBox.Show(msg, "No Connection", MessageBoxButtons.YesNo);
                if (result == DialogResult.No)
                {
                    _connect = false;
                    _ownSender.Close();
                }
            }
            while (_connect)
            {
                Connect();
                Thread.Sleep(500);
            }            
        }

        private bool Connect()
        {
            try
            {
                _ownSender = new Socket(_endPoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                #region Connect with timeout
                IAsyncResult result = _ownSender.BeginConnect(_endPoint.Address, _endPoint.Port, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(_timeout, true);
                if (_ownSender.Connected)
                {
                    _ownSender.EndConnect(result);
                }
                else
                {
                    _ownSender.Close();
                    Output("Failed to connect server.");
                    return false;
                }
                #endregion

                // Connect to Remote EndPoint  
                //_ownSender.Connect(address, port);
                Output("Socket connected to " + _ownSender.RemoteEndPoint);

                //configure Socket
                _ownSender.ReceiveBufferSize = _buffersize;
                _ownSender.SendBufferSize = _buffersize;
                _ownSender.SendTimeout = _timeout;

                var comThread = new Thread(Read);
                comThread.Start();
                comThread.Join();
                _ownSender.Close();
            }
            catch (Exception ex)
            {
                Debug("Connection failed: " + ex);
                return false;
            }
            return true;
        }

        /// <summary>
        /// threaded reading of incoming messages as client
        /// </summary>
        private void Read()
        {
            int timer = 0;
            int sleeptime = 100;
            int timeout = _timeout/sleeptime;            

            while (_connect)
            {
                try
                {
                    byte[] bytes = new byte[_buffersize];
                    int bytesRec = 0;
                    if (!Connectioncheck(_ownSender) || (bytesRec = _ownSender.Receive(bytes)) <= 0)
                    {
                        timer++;
                        if (timer == timeout/2)
                        {
                            Debug("Receiving data failed for: " + timer + " times");
                        }
                        if (timer >= timeout)
                        {
                            Debug("Receiving data failed for: " + timer + " times");
                            Debug("Timed out");
                            return;
                        }                        
                    }
                    else
                    {
                        timer = 0;
                        
                        // Receive the response from the remote device.    
                        
                        Debug("Echoed with = " + bytesRec + " " + bytes);
                        Debug("Echoed: " + Encoding.ASCII.GetString(bytes, 0, bytesRec));

                        Output("Echoed: " + Encoding.ASCII.GetString(bytes, 0, bytesRec));
                    }

                    Thread.Sleep(sleeptime);
                    _ownSender.Send(Encoding.ASCII.GetBytes(""));
                }
                catch (Exception ex)
                {
                    timer++;
                    if (timer == timeout / 2)
                    {
                        Debug("Receiving data failed for: " + timer + " times with error:" + ex.Message);
                    }
                    if (timer >= timeout)
                    {
                        Debug("Receiving data failed for: " + timer + " times with error:" + ex.Message);
                        Debug("Timed out");
                        return;
                    }
                }
            }

        }               
        private void B_send_Click(object sender, EventArgs e)
        {
            try
            {
                /*string _message = txt_message.Text;
                _byteCount = Encoding.ASCII.GetByteCount(_message);
                _sendData=new byte[_byteCount];
                _sendData = Encoding.ASCII.GetBytes(_message);
                _stream = _client.GetStream();
                _stream.Write(_sendData,0,_sendData.Length);
                */
                if (!Connectioncheck(_ownSender))
                {
                    Output("Not connected");
                    return;
                }

                //byte[] bytes = new byte[1024];

                string msg = txt_message.Text;
                Debug("Sending: " + msg);
                // Send the data through the socket.    
                int bytesSent = _ownSender.Send(Encoding.ASCII.GetBytes(msg)); //public int Send(byte[] buffer, int offset, int size, SocketFlags socketFlags);
                Output("Message send: " + msg);

                // Receive the response from the remote device.    
                //int bytesRec = _ownSender.Receive(bytes); //public int Receive(byte[] buffer, int offset, int size, SocketFlags socketFlags);
                //Output("Echoed test = " + Encoding.ASCII.GetString(bytes, 0, bytesRec));

            }
            catch (Exception exception)
            {
                Debug("Failed to send data: " + exception);
            }
        }

        private void B_disconnect_Click(object sender, EventArgs e)
        {
            try
            {
                _connect = false;
                if (!Connectioncheck(_ownSender))
                {
                    Output("Not connected");
                    return;
                }


                // Release the socket.    
                _ownSender.Shutdown(SocketShutdown.Both);
                _ownSender.Close();
                //_stream.Close();
            }

            catch (Exception exception)
            {
                Debug("" + exception);
                return;
            }

            //_client.Close();
            Output("Connection terminated");
        }

        private void B_server_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(TXT_port.Text, out int port))
            {
                Debug("undefined Port");
                return;
            }
            IPAddress address = GetIPAddress();
            if (address == null || address.ToString() == "0.0.0.0")
            {
                Debug("undefined IP");
                return;
            }

            Output("Trying to start Server on: " + address + ":" + port);
            _serverSocket = new TcpListener(address, port);
            var serThread = new Thread(RunServer);
            serThread.Start();
        }
        #region Serverstuff
        private void RunServer()
        {
            try
            {

                _serverSocket.Start();
                Debug("Now listening");
                while (true)
                {
                    _clientSocket = _serverSocket.AcceptTcpClient();
                    var ctThread = new Thread(DoChat);
                    ctThread.Start();
                    Debug("Chat with client at: " + _clientSocket.Client.RemoteEndPoint);
                }


                /*
                var counter = 0;
                while (counter < 1)
                {
                    counter += 1;
                    clientSocket = _serverSocket.AcceptTcpClient();
                    Debug(" >> " + "Client No:" + Convert.ToString(counter) + " started!");
                    HandleClient clithread = new HandleClient();
                    clithread.StartClient(clientSocket, Convert.ToString(counter));
                }*/
            }
            catch (Exception ex)
            {
                Debug("Creating Server failed: " + ex.Message);
            }

        }
        private void DoChat()
        {
            while (true)
            {
                try
                {
                    NetworkStream DataStream = _clientSocket.GetStream(); //Gets the data stream
                    byte[] buffer = new byte[_clientSocket.ReceiveBufferSize]; //Gets required buffer size
                    int Data = DataStream.Read(buffer, 0, _clientSocket.ReceiveBufferSize); //Gets message (encoded)
                    string message = Encoding.ASCII.GetString(buffer, 0, Data); //Decoodes message
                    if (message == "")
                    {
                        Debug("Connection Terminated");
                        return;
                    }
                    Debug(message);
                    var serverResponse = "Baum";
                    var sendBytes = Encoding.ASCII.GetBytes(serverResponse);
                    DataStream.Write(sendBytes, 0, sendBytes.Length);
                    DataStream.Flush();
                    Debug(" >> " + serverResponse);
                    Thread.Sleep(5000);
                }
                catch (Exception ex)
                {
                    Debug("Client error: " + ex);
                }
            }
        }
        private void StopServer()
        {
            _serverSocket?.Stop();
            Debug("Server stopped on");
        }
        #endregion

        private IPAddress GetIPAddress()
        {
            try
            {
                string network = null;
                try
                {
                    network = TXT_ip.Text.Substring(0, TXT_ip.Text.IndexOf('.'));
                }
                catch (Exception)
                {
                    network = "192.168.";
                }
                Debug("Checking for: " + network);

                //Sucht eigene IP Adressen und beschränkt diese auf das lokale Netzwerk
                IPAddress[] addr = Dns.GetHostEntry(Dns.GetHostName()).AddressList;
                for (int i = 0; i < addr.Length; i++)
                {
                    //Debug("Checking: " + addr[i]);
                    if (!addr[i].ToString().Contains(network))
                    {
                        Debug("Skipped: " + addr[i].ToString());
                        List<IPAddress> tmp = new List<IPAddress>(addr);
                        tmp.Remove(addr[i]);
                        addr = tmp.ToArray();
                        i--;
                    }

                }

                string available = "";
                foreach (var t in addr)
                {
                    available += " \n " + t.ToString();
                    Debug("IPs found: " + t);
                }

                //bei mehreren IPs wird nach der gewünschten gefragt
                IPAddress iPAddress = new IPAddress(0);
                if (addr.Length > 1)
                {
                    for (int i = 0; i < addr.Length - 1; i++)
                    {
                        string message = "Several IP Addresses found: " + available + "\n choose:\n" + addr[i] + " ?";
                        DialogResult result = MessageBox.Show(message, "Choose IP", MessageBoxButtons.YesNo);
                        if (result == DialogResult.Yes)
                            iPAddress = addr[i];
                    }
                    if (iPAddress.ToString() == "0.0.0.0")
                    {
                        iPAddress = addr[addr.Length - 1];
                    }
                }
                else
                {
                    iPAddress = addr[0];
                }
                return iPAddress;
            }
            catch (Exception ex)
            {
                Debug("Getting IP failed: " + ex);
                return null;
            }
        }

        private bool Connectioncheck(Socket socket)
        {
            if (socket == null)
            {
                return false;
            }
            if (!socket.Connected)
            {
                return false;
            }
            return true;
        }

    }
}
