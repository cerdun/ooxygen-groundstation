﻿namespace com_test
{
    partial class Client
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Client));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.B_connect = new System.Windows.Forms.ToolStripButton();
            this.B_send = new System.Windows.Forms.ToolStripButton();
            this.B_disconnect = new System.Windows.Forms.ToolStripButton();
            this.B_server = new System.Windows.Forms.ToolStripButton();
            this.TXT_ip = new System.Windows.Forms.TextBox();
            this.TXT_port = new System.Windows.Forms.TextBox();
            this.txt_message = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Servertimer = new System.Windows.Forms.Timer(this.components);
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.B_UDP = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.B_connect,
            this.B_send,
            this.B_disconnect,
            this.B_server,
            this.B_UDP});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(598, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // B_connect
            // 
            this.B_connect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.B_connect.Image = global::com_test.Properties.Resources.connect_icon;
            this.B_connect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_connect.Name = "B_connect";
            this.B_connect.Size = new System.Drawing.Size(23, 22);
            this.B_connect.Text = "B_connect";
            this.B_connect.Click += new System.EventHandler(this.B_connect_Click);
            // 
            // B_send
            // 
            this.B_send.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.B_send.Image = global::com_test.Properties.Resources.send_icon;
            this.B_send.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_send.Name = "B_send";
            this.B_send.Size = new System.Drawing.Size(23, 22);
            this.B_send.Text = "B_send";
            this.B_send.Click += new System.EventHandler(this.B_send_Click);
            // 
            // B_disconnect
            // 
            this.B_disconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.B_disconnect.Image = global::com_test.Properties.Resources.close_icon;
            this.B_disconnect.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_disconnect.Name = "B_disconnect";
            this.B_disconnect.Size = new System.Drawing.Size(23, 22);
            this.B_disconnect.Text = "B_disconnect";
            this.B_disconnect.Click += new System.EventHandler(this.B_disconnect_Click);
            // 
            // B_server
            // 
            this.B_server.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.B_server.Image = ((System.Drawing.Image)(resources.GetObject("B_server.Image")));
            this.B_server.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_server.Name = "B_server";
            this.B_server.Size = new System.Drawing.Size(23, 22);
            this.B_server.Text = "B_server";
            this.B_server.Click += new System.EventHandler(this.B_server_Click);
            // 
            // TXT_ip
            // 
            this.TXT_ip.Location = new System.Drawing.Point(88, 28);
            this.TXT_ip.Name = "TXT_ip";
            this.TXT_ip.Size = new System.Drawing.Size(355, 20);
            this.TXT_ip.TabIndex = 1;
            this.TXT_ip.Text = "192.168.178.70";
            // 
            // TXT_port
            // 
            this.TXT_port.Location = new System.Drawing.Point(481, 28);
            this.TXT_port.Name = "TXT_port";
            this.TXT_port.Size = new System.Drawing.Size(100, 20);
            this.TXT_port.TabIndex = 2;
            this.TXT_port.Text = "12345";
            // 
            // txt_message
            // 
            this.txt_message.Location = new System.Drawing.Point(88, 54);
            this.txt_message.Name = "txt_message";
            this.txt_message.Size = new System.Drawing.Size(493, 20);
            this.txt_message.TabIndex = 3;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(15, 93);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(274, 264);
            this.listBox1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "IP/Hostname";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(449, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Message";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Output";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(295, 93);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(286, 264);
            this.listBox2.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(295, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Output";
            // 
            // B_UDP
            // 
            this.B_UDP.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.B_UDP.Image = ((System.Drawing.Image)(resources.GetObject("B_UDP.Image")));
            this.B_UDP.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.B_UDP.Name = "B_UDP";
            this.B_UDP.Size = new System.Drawing.Size(23, 22);
            this.B_UDP.Text = "B_UDP";
            this.B_UDP.Click += new System.EventHandler(this.B_UDP_Click);
            // 
            // Client
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(598, 373);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.txt_message);
            this.Controls.Add(this.TXT_port);
            this.Controls.Add(this.TXT_ip);
            this.Controls.Add(this.toolStrip1);
            this.Name = "Client";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Client_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton B_connect;
        private System.Windows.Forms.ToolStripButton B_send;
        private System.Windows.Forms.ToolStripButton B_disconnect;
        private System.Windows.Forms.TextBox TXT_ip;
        private System.Windows.Forms.TextBox TXT_port;
        private System.Windows.Forms.TextBox txt_message;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripButton B_server;
        private System.Windows.Forms.Timer Servertimer;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripButton B_UDP;
    }
}

